﻿using System;
using System.Collections.Generic;
using System.Linq;
using CollectionOperations;
using CustomCollection;
using CustomStack;

namespace CollectionViewer.Core
{
    public class CollectionViewer
    {
        public CustomStack<T> CreateStack<T>()
        {
            return new CustomStack<T>();
        }

        public Collection<T> CreateColletion<T>()
        {
            return new Collection<T>();
        }

        public IEnumerable<T> GetIntersection<T>(IEnumerable<T> first, IEnumerable<T> second)
        {
            CollectionValidate(first);
            CollectionValidate(second);

            return Operation.Source.Intersection(first, second);
        }

        public IEnumerable<T> GetUnion<T>(IEnumerable<T> first, IEnumerable<T> second)
        {
            CollectionValidate(first);
            CollectionValidate(second);

            return Operation.Source.Union(first, second);
        }

        public IEnumerable<T> Concatenate<T>(params IEnumerable<T>[] parameters)
        {
            foreach (var enumerable in parameters)
            {
                CollectionValidate(enumerable);
            }

            return Operation.Source.Concatenate(parameters);
        }

        public IEnumerable<int> GetRandomCollection()
        {
            var rnd = new Random();

            for (int i = 0; i < 30; i++)
            {
                yield return rnd.Next(1, 100);
            }
        }

        private void CollectionValidate<T>(IEnumerable<T> collection)
        {
            if (collection == null)
                throw new ArgumentNullException(nameof(collection));
        }
    }
}
